// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

#include "subsystems/DrivetrainSubsystem.h"
#include "Constants.h"
#include <frc/smartdashboard/SmartDashboard.h>
#include <pathplanner/lib/auto/AutoBuilder.h>
#include <pathplanner/lib/util/HolonomicPathFollowerConfig.h>
#include <pathplanner/lib/util/PIDConstants.h>
#include <pathplanner/lib/util/ReplanningConfig.h>
#include <frc/DriverStation.h>
#include <frc/geometry/Rotation2d.h>

using namespace pathplanner;

DrivetrainSubsystem::DrivetrainSubsystem() {
  // Implementation of subsystem constructor goes here.
    // Configure the AutoBuilder last
    /*AutoBuilder::configureHolonomic(
        [this](){ return GetPose(); }, // Robot pose supplier
        [this](frc::Pose2d pose){ ResetPose(pose); }, // Method to reset odometry (will be called if your auto has a starting pose)
        [this](){ return GetRobotRelativeSpeeds(); }, // ChassisSpeeds supplier. MUST BE ROBOT RELATIVE
        [this](frc::ChassisSpeeds speeds){ Drive(speeds.vx, speeds.vy, speeds.omega, false); }, // Method that will drive the robot given ROBOT RELATIVE ChassisSpeeds
        HolonomicPathFollowerConfig( // HolonomicPathFollowerConfig, this should likely live in your Constants class
            PIDConstants(5.0, 0.0, 0.0), // Translation PID constants
            PIDConstants(5.0, 0.0, 0.0), // Rotation PID constants
            4.5_mps, // Max module speed, in m/s
            0.4_m, // Drive base radius in meters. Distance from robot center to furthest module.
            ReplanningConfig() // Default path replanning config. See the API for the options here
        ),
        []() {
            // Boolean supplier that controls when the path will be mirrored for the red alliance
            // This will flip the path being followed to the red side of the field.
            // THE ORIGIN WILL REMAIN ON THE BLUE SIDE

            auto alliance = frc::DriverStation::GetAlliance();
            if (alliance) {
                return alliance.value() == frc::DriverStation::Alliance::kRed;
            }
            return false;
        },
        this // Reference to this subsystem to set requirements
    );*/
}

frc2::CommandPtr DrivetrainSubsystem::ExampleMethodCommand() {
  // Inline construction of command goes here.
  // Subsystem::RunOnce implicitly requires `this` subsystem.
  return RunOnce([/* this */] { /* one-time action goes here */ });
}

bool DrivetrainSubsystem::ExampleCondition() {
  // Query some boolean state, such as a digital sensor.
  return false;
}

void DrivetrainSubsystem::UpdateOdometry() {
  m_odometry.Update(m_gyro.GetRotation2d(),
                    {m_frontLeft.GetPosition(), m_frontRight.GetPosition(),
                     m_backLeft.GetPosition(), m_backRight.GetPosition()});
}

void DrivetrainSubsystem::Periodic() {
  // Implementation of subsystem periodic method goes here.
  frc::SmartDashboard::PutNumber("LampreyVoltageFL", m_frontLeft.GetLampreyVoltage());
  frc::SmartDashboard::PutNumber("LampreyVoltageFR", m_frontRight.GetLampreyVoltage());
  frc::SmartDashboard::PutNumber("LampreyVoltageBL", m_backLeft.GetLampreyVoltage());
  frc::SmartDashboard::PutNumber("LampreyVoltageBR", m_backRight.GetLampreyVoltage());
  frc::SmartDashboard::PutNumber("LampreyRadiansFL", m_frontLeft.getAbsolutePositionRadians());
  frc::SmartDashboard::PutNumber("LampreyRadiansFR", m_frontRight.getAbsolutePositionRadians());
  frc::SmartDashboard::PutNumber("LampreyRadiansBL", m_backLeft.getAbsolutePositionRadians());
  frc::SmartDashboard::PutNumber("LampreyRadiansBR", m_backRight.getAbsolutePositionRadians());
  frc::SmartDashboard::PutNumber("Gyro Angle", m_gyro.GetRotation2d().Degrees().value());
}

void DrivetrainSubsystem::Drive(units::meters_per_second_t xSpeed,
                       units::meters_per_second_t ySpeed,
                       units::radians_per_second_t rot, bool fieldRelative) {
  auto states =
      m_kinematics.ToSwerveModuleStates(frc::ChassisSpeeds::Discretize(
          fieldRelative ? frc::ChassisSpeeds::FromFieldRelativeSpeeds(
                              xSpeed, ySpeed, rot, m_gyro.GetRotation2d())
                        : frc::ChassisSpeeds{xSpeed, ySpeed, rot},
          0.02_s));

  m_kinematics.DesaturateWheelSpeeds(&states, kMaxSpeed);

  auto [fl, fr, bl, br] = states;

  m_frontLeft.SetDesiredState(fl);
  m_frontRight.SetDesiredState(fr);
  m_backLeft.SetDesiredState(bl);
  m_backRight.SetDesiredState(br);
}

frc::Pose2d DrivetrainSubsystem::GetPose() {
  return m_odometry.GetPose();
}

void DrivetrainSubsystem::ResetPose(frc::Pose2d pose) {
    m_odometry.ResetPosition(
        m_gyro.GetRotation2d().RotateBy(frc::Rotation2d(180_deg)),
        {m_frontLeft.GetPosition(),
          m_frontRight.GetPosition(),
          m_backLeft.GetPosition(),
          m_backRight.GetPosition()},
        pose);
}

/*frc::ChassisSpeeds DrivetrainSubsystem::GetRobotRelativeSpeeds() {
  return m_kinematics.ToChassisSpeeds(m_frontLeft.GetState(), m_frontRight.GetState(), m_backLeft.GetState(), m_backRight.GetState()) ;
}*/
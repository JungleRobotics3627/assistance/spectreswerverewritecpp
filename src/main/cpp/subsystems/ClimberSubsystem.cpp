#include "subsystems/ClimberSubsystem.h"

ClimberSubsystem::ClimberSubsystem() {
    m_rightClimber.SetInverted(true);
    m_rightClimber.Follow(m_leftClimber);
}

//void ClimberSubsystem::ToggleClimber() {}
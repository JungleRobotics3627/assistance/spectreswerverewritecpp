// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

#include "subsystems/ShooterSubsystem.h"
#include <frc/smartdashboard/SmartDashboard.h>
#include <networktables/NetworkTable.h>

ShooterSubsystem::ShooterSubsystem() {
  // Implementation of subsystem constructor goes here.
}



void ShooterSubsystem::Periodic() {
  // Implementation of subsystem periodic method goes here.
  tAngleHorizontal = (table->GetNumber("tx",0.0)/10);
  tAngleVertical = (table->GetNumber("ty",0.0)/10);
  tID = table->GetNumber("tid",3.0);
  
  
  
  /** @param ShooterPosition The current position of the shooter */
  double ShooterPosition = ShooterEncoder.GetPosition();
  /** @param ShooterSetpoint The wanted setpoint for the shooter */
  double ShooterSetpoint = std::clamp(ShooterPosition + (-tAngleVertical),-4.0,14.0);
  /** @param TiltSpeed The speed at which the shooter should tilt */
  double TiltSpeed = TiltPID.Calculate(ShooterPosition,ShooterSetpoint);
  
  // Set shooter motor speed, clamp values to 20% to prevent damage to the motor and mechanism
  m_ShooterPivotMotor.Set(std::clamp(TiltSpeed, -0.2, 0.2));

  
}

void ShooterSubsystem::ToggleIntake() {
    m_IntakeSolenoid.Toggle();
}

void ShooterSubsystem::RunIntakeMotors() {
    m_IntakeMotor.Set(0.1);
    if (m_IntakeSolenoid.Get() == frc::DoubleSolenoid::kForward) {
      m_IndexerMotor.Set(0.1);
    }
}

void ShooterSubsystem::ShootNote() {
    m_ShooterMotor.Set(0.5);
}

//void ShooterSubsystem::SetOuttakePosition() {} To be implemented later

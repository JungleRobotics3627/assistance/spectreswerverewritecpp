// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

#include "RobotContainer.h"

#include <frc2/command/button/Trigger.h>
#include <frc2/command/RunCommand.h>
#include "commands/Autos.h"

RobotContainer::RobotContainer() {
  // Initialize all of your commands and subsystems here
  m_DriveSystem.SetDefaultCommand(frc2::RunCommand([this] {m_DriveSystem.Drive(
    -units::meters_per_second_t{frc::ApplyDeadband(m_driverController.GetLeftX(), 0.05)},
    -units::meters_per_second_t{frc::ApplyDeadband(m_driverController.GetLeftY(), 0.05)},
    -units::radians_per_second_t{frc::ApplyDeadband(m_driverController.GetRightX(), 0.05)},
    false
  );}, {&m_DriveSystem}));
  // Configure the button bindings
  ConfigureBindings();
}

void RobotContainer::ConfigureBindings() {
  // Schedule `something` when the Xbox controller's B button is
  // pressed
  //m_driverController.B().WhileTrue(something);
}

frc2::CommandPtr RobotContainer::GetAutonomousCommand() {
  // An example command will be run in autonomous
  return autos::ExampleAuto(&m_subsystem);
}

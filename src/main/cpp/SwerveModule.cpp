// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

#include "SwerveModule.h"
#include <numbers>
#include <frc/geometry/Rotation2d.h>
#include <vector>
#include <cmath>
#include <wpi/detail/value_t.h>
 frc::Rotation2d offset = frc::Rotation2d();

SwerveModule::SwerveModule(const int driveMotorChannel,
                           const int turningMotorChannel,
                           const int turningEncoderChannel,
                           const int VoltageType)
    : m_driveMotor(driveMotorChannel, rev::CANSparkMax::MotorType::kBrushless),
      m_turningMotor(turningMotorChannel, rev::CANSparkMax::MotorType::kBrushless),
      m_turningEncoder(turningEncoderChannel),
      VoltageType(VoltageType) {
  // Set the distance per pulse for the drive encoder. We can simply use the
  // distance traveled for one rotation of the wheel divided by the encoder
  // resolution.
  m_driveEncoder.SetPositionConversionFactor(2 * std::numbers::pi * kWheelRadius / kDriveGearReduction);
  m_driveEncoder.SetVelocityConversionFactor(2 * std::numbers::pi * kWheelRadius /(kDriveGearReduction * kSecondsinMinute));

  // Limit the PID Controller's input range between -pi and pi and set the input
  // to be continuous.
  m_turningPIDController.EnableContinuousInput(
      -units::radian_t{std::numbers::pi}, units::radian_t{std::numbers::pi});
}

double SwerveModule::getAbsolutePositionRadians() const {
    if (VoltageType == 1){
      return (2*std::numbers::pi-((m_turningEncoder.GetVoltage()/5)*(2*std::numbers::pi)));
    } else {
      return (2*std::numbers::pi-((m_turningEncoder.GetVoltage())/3.3*(2*std::numbers::pi)));
    }
  }
double SwerveModule::getAbsoluteWheelHeading(double currentheading) const {
  frc::Rotation2d heading = frc::Rotation2d(units::radian_t{currentheading});
  double turnpositionreading = m_turnEncoderRelative.GetPosition();
  frc::Rotation2d turnposition =
        frc::Rotation2d(units::angle::turn_t{turnpositionreading / 40.444});
  if (offset == frc::Rotation2d() && heading.Radians() != units::radian_t{0}) {
      offset = heading-turnposition;
    }
    return -1;
  }

frc::SwerveModuleState SwerveModule::GetState() const {
  return {units::meters_per_second_t{m_driveEncoder.GetVelocity()},
          units::radian_t{SwerveModule::getAbsolutePositionRadians()}};
}

frc::SwerveModulePosition SwerveModule::GetPosition() const {
  return {units::meter_t{m_driveEncoder.GetPosition()},
          units::radian_t{SwerveModule::getAbsolutePositionRadians()}};
}

void SwerveModule::SetDesiredState(
    const frc::SwerveModuleState& referenceState) {
  
  frc::Rotation2d encoderRotation{
      units::radian_t{SwerveModule::getAbsolutePositionRadians()}};

  // Optimize the reference state to avoid spinning further than 90 degrees
  auto state =
      frc::SwerveModuleState::Optimize(referenceState, encoderRotation);

  // Scale speed by cosine of angle error. This scales down movement
  // perpendicular to the desired direction of travel that can occur when
  // modules change directions. This results in smoother driving.
  state.speed *= (state.angle - encoderRotation).Cos();

  // Calculate the drive output from the drive PID controller.
  const auto driveOutput = m_drivePIDController.Calculate(
      m_driveEncoder.GetVelocity(), state.speed.value());

  const auto driveFeedforward = m_driveFeedforward.Calculate(state.speed);

  // Calculate the turning motor output from the turning PID controller.
  const auto turnOutput = m_turningPIDController.Calculate(
      units::radian_t{SwerveModule::getAbsolutePositionRadians()}, state.angle.Radians());

  const auto turnFeedforward = m_turnFeedforward.Calculate(
      m_turningPIDController.GetSetpoint().velocity);

  // Set the motor outputs.
  m_driveMotor.SetVoltage(units::volt_t{driveOutput} + driveFeedforward);
  m_turningMotor.SetVoltage(units::volt_t{turnOutput} + turnFeedforward);

}

double SwerveModule::GetLampreyVoltage() {
    return m_turningEncoder.GetVoltage();
}


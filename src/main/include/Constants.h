// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

#pragma once
#include <math.h>
#include <units/length.h>
#include <frc/geometry/Translation2d.h>
#include <frc/kinematics/SwerveDriveKinematics.h>
#include <units/length.h>
#include <units/angular_velocity.h>
#include <units/velocity.h>

/**
 * The Constants header provides a convenient place for teams to hold robot-wide
 * numerical or boolean constants.  This should not be used for any other
 * purpose.
 *
 * It is generally a good idea to place constants into subsystem- or
 * command-specific namespaces within this header, which can then be used where
 * they are needed.
 */

namespace OperatorConstants {
    inline constexpr int kDriverControllerPort = 0;
    class Constants{
        public:
        class ModuleConstants{
            public:
                const units::velocity::meters_per_second_t kMaxSpeed = 3_mps;
                const units::angular_velocity::radians_per_second_t kMaxAngularSpeed = M_PI * 1_rad_per_s;
                const units::length::inch_t kWheelRadius = 2_in;
                const units::length::inch_t kWheelDiameter = kWheelRadius * 2;
                const units::length::inch_t kWheelCircumference = 2 * M_PI * kWheelRadius;
                constexpr static double kDrivingGearRatio = 9;

                

                
            private:


                
        };
        class TimeConstants{
            constexpr static double SecondsInMinutes = 60;
        };
        class HIDConstants {
            constexpr static double kJoystickDeadband = 0.09;
        };
    };
}; // namespace
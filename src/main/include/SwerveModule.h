// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

#pragma once

#include <numbers>

#include <frc/Encoder.h>
#include <frc/controller/PIDController.h>
#include <frc/controller/ProfiledPIDController.h>
#include <frc/controller/SimpleMotorFeedforward.h>
#include <frc/kinematics/SwerveModulePosition.h>
#include <frc/kinematics/SwerveModuleState.h>
#include <frc/motorcontrol/PWMSparkMax.h>
#include <units/angular_velocity.h>
#include <units/time.h>
#include <units/velocity.h>
#include <units/voltage.h>

#include <rev/CANSparkMax.h>
#include <rev/SparkRelativeEncoder.h>

#include <frc/geometry/Rotation2d.h>
#include <frc/kinematics/SwerveModulePosition.h>
#include <frc/kinematics/SwerveModuleState.h>
#include <rev/CANSparkMax.h>
#include <rev/SparkAbsoluteEncoder.h>
#include <rev/SparkPIDController.h>
#include <frc/AnalogInput.h>
static const int numEncoders = 4;
static const int numDegrees = 33;
class SwerveModule {
 public:
  SwerveModule(int driveMotorChannel, int turningMotorChannel,
               int turningEncoderChannel, int VoltageType);
  double getAbsolutePositionRadians() const;
  double getAbsoluteWheelHeading(double currentvoltage) const;
  frc::SwerveModuleState GetState() const;
  frc::SwerveModulePosition GetPosition() const;
  void SetDesiredState(const frc::SwerveModuleState& state);
  double GetLampreyVoltage();
  void SetPID(double P, double I, double D);

 private:
  static constexpr double kWheelRadius = 0.0508;
  static constexpr double kDriveGearReduction = 9.0;
  static constexpr double kSecondsinMinute = 60.0;

  int VoltageTypeInput;

  static constexpr auto kModuleMaxAngularVelocity =
      std::numbers::pi * 1_rad_per_s;  // radians per second
  static constexpr auto kModuleMaxAngularAcceleration =
      std::numbers::pi * 1_rad_per_s / 1_s;  // radians per second^2

rev::CANSparkMax m_driveMotor;

rev::CANSparkMax m_turningMotor;

rev::SparkRelativeEncoder m_driveEncoder =
    m_driveMotor.GetEncoder();
rev::SparkRelativeEncoder m_turnEncoderRelative =
    m_turningMotor.GetEncoder();

frc::AnalogInput m_turningEncoder;

int VoltageType;
double moduleP;
double moduleI;
double moduleD;

frc::PIDController m_drivePIDController{1.0, 0, 0};
frc::ProfiledPIDController<units::radians> m_turningPIDController{
    0.5,
    0.0,
    0.0,
    {kModuleMaxAngularVelocity, kModuleMaxAngularAcceleration}};

frc::SimpleMotorFeedforward<units::meters> m_driveFeedforward{1_V,
                                                              3_V / 1_mps};
  frc::SimpleMotorFeedforward<units::radians> m_turnFeedforward{
      0.6_V, 0.3_V / 1_rad_per_s};
      
};

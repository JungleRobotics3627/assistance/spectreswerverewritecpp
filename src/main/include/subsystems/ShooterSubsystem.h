#pragma once

#include <frc/Compressor.h>
#include <frc/DoubleSolenoid.h>
#include <rev/CANSparkMax.h>
#include <frc2/command/SubsystemBase.h>
#include <frc/controller/PIDController.h>

#include <frc/smartdashboard/SmartDashboard.h>
#include <frc/smartdashboard/SendableChooser.h>
#include <networktables/NetworkTable.h>
#include <networktables/NetworkTableInstance.h>

class ShooterSubsystem : public frc2::SubsystemBase {
    public:
        ShooterSubsystem();
        /**
         * Toggles the intake piston
        */
        void ToggleIntake();
        /**
         * Runs the intake wheel motors
        */
        void RunIntakeMotors();
        /**
         * Shoots the note
        */
        void ShootNote();

       // void SetOuttakePosition(); To be implemented later

        void Periodic() override;


    private:
        // Initialize motor controllers, solenoid, and compressor for the Intake //
        //                                                                       //

        // @param m_Compressor The solenoid that controls the intake piston
        frc::Compressor m_Compressor{3, frc::PneumaticsModuleType::REVPH};

        // @param m_IntakeSolenoid The solenoid that controls the intake piston
        frc::DoubleSolenoid m_IntakeSolenoid{3, frc::PneumaticsModuleType::REVPH, 1, 2};

        // @param m_IntakeMotor The motor that controls the intake spin
        rev::CANSparkMax m_IntakeMotor{14, rev::CANSparkLowLevel::MotorType::kBrushless};

        
        // Initialize motor controllers and solenoids for the Shooter
        //

        //@param m_ShooterMotor The motor that controls the shooter spin
        rev::CANSparkMax m_ShooterMotor{15, rev::CANSparkLowLevel::MotorType::kBrushless};

        //@param m_ShooterMotor The motor that controls the shooter spin
        rev::CANSparkMax m_IndexerMotor{16, rev::CANSparkLowLevel::MotorType::kBrushless};

        //@param m_ShooterPivotMotor The motor that controls the shooter tilt
        rev::CANSparkMax m_ShooterPivotMotor{17, rev::CANSparkLowLevel::MotorType::kBrushless};

        //@param ShooterEncoder The encoder that measures the angle of the shooter tilt
        rev::SparkRelativeEncoder ShooterEncoder = m_ShooterPivotMotor.GetEncoder();


        // Initialize vision values for the limelight //
        //                                            //

        // @param tAngleHorizontal The horizontal angle displacement of the limelight (tx)
        double tAngleHorizontal = 0;
        // @param tAngleVertical The vertical angle displacement of the limelight (ty)
        double tAngleVertical = 0;
        // @param tID The ID of the target that the limelight is tracking (tid)
        double tID;

        /** NetworkTable table for limelight values (tx,ty,tid)
         * @param table The table that contains the limelight values
         * @param tx The horizontal angle displacement of the limelight
         * @param ty The vertical angle displacement of the limelight
         * @param tid The ID of the target that the limelight is tracking
         */
        std::shared_ptr<nt::NetworkTable> table = nt::NetworkTableInstance::GetDefault().GetTable("limelight");

        // PID Controller for the shooter tilt
        frc::PIDController TiltPID{0.6,0,0.001};

};
#pragma once

#include <rev/CANSparkMax.h>
#include <frc2/command/SubsystemBase.h>

class ClimberSubsystem : public frc2::SubsystemBase {
    public:
        ClimberSubsystem();

        void ToggleClimber();


    private:
        rev::CANSparkMax m_leftClimber{1, rev::CANSparkLowLevel::MotorType::kBrushless};
        rev::CANSparkMax m_rightClimber{1, rev::CANSparkLowLevel::MotorType::kBrushless};

};
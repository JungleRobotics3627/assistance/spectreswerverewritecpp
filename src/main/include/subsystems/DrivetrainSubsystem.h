// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

#pragma once

#include <frc2/command/CommandPtr.h>
#include <frc2/command/SubsystemBase.h>
#include <numbers>

#include <frc/AnalogGyro.h>
#include <ctre/phoenix6/Pigeon2.hpp>
#include <frc/geometry/Translation2d.h>
#include <frc/kinematics/SwerveDriveKinematics.h>
#include <frc/kinematics/SwerveDriveOdometry.h>

#include "SwerveModule.h"

class DrivetrainSubsystem : public frc2::SubsystemBase {

 public:
  DrivetrainSubsystem();

  void Drive(units::meters_per_second_t xSpeed,
             units::meters_per_second_t ySpeed, units::radians_per_second_t rot,
             bool fieldRelative);
  /**
   * Example command factory method.
   */
  frc2::CommandPtr ExampleMethodCommand();

  /**
   * An example method querying a boolean state of the subsystem (for example, a
   * digital sensor).
   *
   * @return value of some boolean subsystem state, such as a digital sensor.
   */
  bool ExampleCondition();
  
  void UpdateOdometry();
  /**
   * Will be called periodically whenever the CommandScheduler runs.
   */
  void Periodic() override;

  frc::Pose2d GetPose();

  void ResetPose (frc::Pose2d pose);
  
  //frc::ChassisSpeeds DrivetrainSubsystem::GetRobotRelativeSpeeds() {};
  
  static constexpr units::meters_per_second_t kMaxSpeed =
      3.0_mps;  // 3 meters per second
  static constexpr units::radians_per_second_t kMaxAngularSpeed{
      std::numbers::pi};  // 1/2 rotation per second

 private:
  // Components (e.g. motor controllers and sensors) should generally be
  // declared private and exposed only through public methods.
  frc::Translation2d m_frontLeftLocation{+0.3683_m, +0.3683_m};
  frc::Translation2d m_frontRightLocation{+0.3683_m, -0.3683_m};
  frc::Translation2d m_backLeftLocation{-0.3683_m, +0.3683_m};
  frc::Translation2d m_backRightLocation{-0.3683_m, -0.3683_m};

  SwerveModule m_frontLeft{11,12,0,1};
  SwerveModule m_frontRight{6,5,3,2};
  SwerveModule m_backLeft{13,10,2,1};
  SwerveModule m_backRight{8,7,1,2};

  ctre::phoenix6::hardware::Pigeon2 m_gyro{9};

  frc::SwerveDriveKinematics<4> m_kinematics{
      m_frontLeftLocation, m_frontRightLocation, m_backLeftLocation,
      m_backRightLocation};

  frc::SwerveDriveOdometry<4> m_odometry{
      m_kinematics,
      m_gyro.GetRotation2d().RotateBy(frc::Rotation2d(180_deg)),
      {m_frontLeft.GetPosition(), m_frontRight.GetPosition(),
       m_backLeft.GetPosition(), m_backRight.GetPosition()}};

  double ModuleP;
  double ModuleI;
  double ModuleD;
};
